﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InputMode
{
    NONE,
    TELEPORT,
    WALK,
    FURNITURE,
    TRANSLATE,
    ROTATE,
    SCALE,
    DRAG
}

public class Player : MonoBehaviour
{
    [SerializeField]
    private float playerSpeed = 3.0f;

    public static Player instance;

    public InputMode activeMode = InputMode.NONE;

    public Object activeFurniturePrefab;

    private void Awake()
    {
        if(instance != null)
        {
            GameObject.Destroy(instance.gameObject);
        }
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        TryWalk();
    }

    public void TryWalk()
    {
        if(Input.GetMouseButton(0) && activeMode == InputMode.WALK)
        {
            Vector3 forward = Camera.main.transform.forward;
            float y = transform.position.y;
            Vector3 newPosition = transform.position + forward * Time.deltaTime * playerSpeed;
            newPosition.y = y;
            transform.position = newPosition;
        }
    }
}
