﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GazeSystem : MonoBehaviour
{
    public GameObject reticle;
    public Color inactiveReticleColor = Color.gray;
    public Color activeReticleColor = Color.green;
    private GazeableObject currentGazeObject;
    private GazeableObject currentSelectedObject;
    private RaycastHit lastHit;

    // Start is called before the first frame update
    void Start()
    {
        SetReticalColor(inactiveReticleColor);
    }

    // Update is called once per frame
    void Update()
    {
        ProcessGaze();
        CheckForInput(lastHit);
    }

    public void ProcessGaze()
    {
        Ray raycastRay = new Ray(transform.position, transform.forward);
        RaycastHit hitInfo;
        Debug.DrawRay(raycastRay.origin, raycastRay.direction * 100);
        if(Physics.Raycast(raycastRay, out hitInfo))
        {
            //check if the object is interactable
            GameObject hitObj = hitInfo.collider.gameObject;
            GazeableObject gazeObj = hitObj.GetComponentInParent<GazeableObject>();
            if (gazeObj != null)
            {
                if (gazeObj != currentGazeObject)
                {
                    ClearCurrentObject();
                    currentGazeObject = gazeObj;
                    currentGazeObject.OnGazeEnter(hitInfo);
                    SetReticalColor(activeReticleColor);
                }
                else
                {
                    currentGazeObject.OnGaze(hitInfo);
                }
                lastHit = hitInfo;
            }
            else
            {
                ClearCurrentObject();
            }
        }
        else
        {
            ClearCurrentObject();
        }
    }

    private void SetReticalColor(Color reticleColor)
    {
        reticle.GetComponent<Renderer>().material.SetColor("_Color", reticleColor);
    }

    private void CheckForInput(RaycastHit hitInfo)
    {
        //check for down
        if (Input.GetMouseButtonDown(0) && currentGazeObject != null)
        {
            currentSelectedObject = currentGazeObject;
            currentSelectedObject.OnPress(hitInfo);
        }

        //check for hold
        else if(Input.GetMouseButton(0) && currentSelectedObject != null)
        {
            currentSelectedObject.OnHold(hitInfo);
        }

        //check for release
        else if(Input.GetMouseButtonUp(0) && currentSelectedObject != null)
        {
            currentSelectedObject.OnRelease(hitInfo);
            currentSelectedObject = null;
        }
    }

    private void ClearCurrentObject()
    {
        if (currentGazeObject != null)
        {
            currentGazeObject.OnGazeExit();
            SetReticalColor(inactiveReticleColor);
            currentGazeObject = null;
        }
    }
}
